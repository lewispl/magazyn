<?php


namespace Lewis\AdminBundle\Controller;

use Lewis\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;


class UserController extends Controller
{
    /**
     * @Route("/users", name="lewis_magazyn_users")
     *
     */
    public function usersAction() {
        /*
        $users = $this->getDoctrine()
            ->getRepository('LewisAdminBundle:User')
            ->findAll();
        */
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();
        if (!$users) {
            throw $this->createNotFoundException(
                'Nie znaleziono użytkowników'
            );
        }
        return $this->render('LewisAdminBundle:Admin:users.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/profile/edit/{id}", name="lewis_magazyn_saveuser")
     * @Method("POST|GET")
     */
    public function profileEditAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LewisAdminBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        
        //$user = new User();
        //$userManager = $this->get('fos_user.user_manager');

        //$get_user = $userManager->findUserBy(array('id'=>$id));


        $form = $this->createFormBuilder($entity)
            ->add('name', 'text', array(
                'label'=> 'Imię',
                'required' => false,
                'constraints' => array(
                    new Assert\Regex(array(
                        'pattern' => '/^[A-Za-z]+$/',
                        'message' => 'Podaj poprawne imię'
                    ))
                )
            ))
            ->add('surname', 'text', array(
                'label'=> 'Nazwisko',
                'required' => false
            ))
            ->add('username', 'text', array(
                'label'=> 'Login',
                'required' => true
            ))
            ->add('email', 'email', array(
                'label' => 'Email'
            ))
            ->add('enabled', 'choice', array(
                'choices'  => array(0 => 'Nie', 1 => 'Tak'),
                'expanded' => true,
                'required' => true,
                'data' => $entity->isEnabled() ? '1' : '0'
            ))
            ->add('save', 'submit', array('label' => 'Zapisz'))
            ->getForm();

        $form->setData($entity);

        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $output = array();
                $response = new Response();
                $output[] = array('success' => true);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode($output));
                return $response;
            } else {
                $errors = $form->getErrorsAsString(); // return array of errors
                $response = new Response();
                $output[] = array('error' => $errors); // the first error message
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode($output));
                return $response;
            }
        }
        return $this->render('LewisAdminBundle:Admin:user_edit.html.twig', array('form'=>$form->createView(),'entity' => $entity));
    }
    
    /**
     * @Route("/profile/delete/{id}", name="lewis_magazyn_deleteuser")
     * 
     */
    public function userDeleteAction($id) 
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LewisAdminBundle:User')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Nie znaleziono użytkownika.');
        }
        
        $em->remove($entity);
        $em->flush();
        
        return $this->redirectToRoute('lewis_magazyn_users');
    }
}