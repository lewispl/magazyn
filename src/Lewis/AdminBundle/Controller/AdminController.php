<?php

namespace Lewis\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * @Route("/login", name="lewis_magazyn_login")
     *
     */
    public function loginAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            // redirect authenticated users to homepage
            return $this->redirect($this->generateUrl('lewis_magazyn_index'));
        }
        else
        {
            return $this->render('LewisAdminBundle:Security:login.html.twig');
        }
    }
    /**
     * @Route("/", name="lewis_magazyn_index")
     *
     */
    public function indexAction()
    {
        return $this->render('LewisAdminBundle:Admin:index.html.twig');
    }


}
