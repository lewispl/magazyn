<?php

namespace Lewis\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LewisAdminBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
